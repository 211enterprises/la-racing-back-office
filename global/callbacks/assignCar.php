<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	$c = makeSQLSafe($mysqli,$_POST['c']);
	$r = makeSQLSafe($mysqli,$_POST['r']);
	$carID = makeSQLSafe($mysqli,$_POST['car']);
	
	$carQuery = $mysqli->query("SELECT * FROM `LARX_race_cars` WHERE `id` = '$carID' LIMIT 1");
	if($carQuery->num_rows == 1) {
		$car = $carQuery->fetch_array();
		//FETCH RACER
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$r' LIMIT 1");
		if($racer = $racerQuery->fetch_array()) {
			
			//CHECK IF CAMERA IS ACTIVE
			if($car['car_camera'] == 0 && $racer['race_video'] == 1) {	
				echo '{"response" : "noncamera"}';
			} else {
				//FETCH ASSIGNED RECORDS
				$assignQuery = $mysqli->query("SELECT * FROM `LARX_class_assign` WHERE `class_id` = '$c' AND `racer_id` = '$r' LIMIT 1");
				if($assignQuery->num_rows == 1) {
					$mysqli->query("UPDATE `LARX_class_assign` SET `car_id` = '$carID' WHERE `class_id` = '$c' AND `racer_id` = '$r'");
					echo '{"response" : "updated"}';
				} else {
					$mysqli->query("INSERT INTO `LARX_class_assign` (`class_id`,`racer_id`,`car_id`,`timestamp`) VALUES ('$c','$r','$carID',NOW())");
					echo '{"response" : "assigned"}';
				}
			}
			
		}	
	}


?>