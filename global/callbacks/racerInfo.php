<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	$r = makeSQLSafe($mysqli,$_POST['r']);

	$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` WHERE `id` = '$r' LIMIT 1");
	if($racerQuery->num_rows == 1) {
		$racer = $racerQuery->fetch_array();
		echo '{"response":"success", "id":"'.$racer['id'].'", "firstName":"'.$racer['first_name'].'", "lastName":"'.$racer['last_name'].'", "address":"'.$racer['address'].'", "city":"'.$racer['city'].'",
		 "state":"'.$racer['state'].'", "zip":"'.$racer['zip_code'].'", "phone":"'.$racer['phone'].'", "email":"'.$racer['email_address'].'", "flagged":"'.$racer['red_flag'].'"}';
	} else {
		echo '{"response":"fail"}';
	}
?>