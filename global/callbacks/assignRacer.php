<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	$c = makeSQLSafe($mysqli,$_POST['c']);
	$r = makeSQLSafe($mysqli,$_POST['r']);
	
	if(isset($_POST['c']) && isset($_POST['r'])) {
		$classQuery = $mysqli->query("SELECT `id`,`class_hash` FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		if($classQuery->num_rows == 1) {
			$class = $classQuery->fetch_array();
			$classID = $class['id'];
			
			//Get Racer Pass
			$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `pass_hash` = '$r' LIMIT 1");
			if($passQuery->num_rows == 1) {
				$pass = $passQuery->fetch_array();
				$passClass = $pass['class_id'];
				if($passClass == 0) {
					$mysqli->query("UPDATE `LARX_race_passes` SET `class_id` = '$classID' WHERE `pass_hash` = '$r' LIMIT 1");
				}
			}
		}
	}

$passQuery->close();
$classQuery->close();
?>