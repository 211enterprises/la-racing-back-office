	
	$( document ).bind( "mobileinit", function() {
		$.mobile.pageLoadErrorMessage = true;
		$.mobile.pageLoadErrorMessage = "Error Loading Page.";
	});
	
	$(document).bind("pageinit",function() {
		//CHECK COOKIE
		if(window.location.pathname == "/office/") {
			if($.cookie("officeUser") != null) {
				window.location = "/office/schedule/";
			}
		} else {
			if($.cookie("officeUser") == null) {
				window.location = "/office/";
			}
		}
		
		//AJAX LOGIN FORM
		$('button#loginBtn').click(function() {
			var loginUser = $('input[name=username]').val(),
				loginPass = $('input[name=password]').val();
			
			$.ajax({
				type: "POST",
				url: "/office/global/callbacks/login.php",
				data: "u="+loginUser+"&p="+loginPass,
				success: function(data) {
					var auth = $.parseJSON(data);
					if(auth.response == "success") {
						$.cookie("officeUser",""+auth.userID+"", {expires:30, path: '/office/', domain: 'laracingx.com'});
						window.location = '/office/schedule/';
					} else {
						$('div.loginError').html('Login invalid. Try again.');
						$('div.loginError').slideDown();
						$.removeCookie("officeUser", {path: '/office/', domain: 'laracingx.com'});
					}
				},
				error: function() {
					$('div.loginError').html('An error has occuried. Try again.');
					$('div.loginError').slideDown();
				}
			});
			
			return false;
		});
		
		//LOGOUT
		$('a#logout').click(function() {
			$.removeCookie("officeUser", {path: '/office/', domain: 'laracing.com'});
			window.location = "/office/";
		});
		
		
		//------------------------------RACERS----------------------------------//
		//ADD RACER
		$('button#addRacerBtn').click(function() {
			var newRacerFirstName = $('input[name=firstName]'),
				newRacerLastName = $('input[name=lastName]'),
				newRacerAddress = $('input[name=address]'),
				newRacerCity = $('input[name=city]'),
				newRacerState = $('input[name=state]'),
				newRacerZip = $('input[name=zipCode]'),
				newRacerPhone = $('input[name=phone]'),
				newRacerEmail = $('input[name=email]');
			
			if(newRacerFirstName.val() == "") {newRacerFirstName.css("background","red");} else {newRacerFirstName.css("background","white");};
			if(newRacerLastName.val() == "") {newRacerLastName.css("background","red");} else {newRacerLastName.css("background","white");};
			if(newRacerPhone.val() == "") {newRacerPhone.css("background","red");} else {newRacerPhone.css("background","white");};
			if(newRacerEmail.val() == "") {newRacerEmail.css("background","red");} else {newRacerEmail.css("background","white");};
			
			if(newRacerFirstName.val() != "" && newRacerLastName.val() != "" && newRacerPhone.val() != "" && newRacerEmail.val() != "") {
				$.ajax({
					type: "post",
					url: "/office/racers/newRacer.php",
					data: "fname="+encodeURIComponent(newRacerFirstName.val())+"&lname="+encodeURIComponent(newRacerLastName.val())+"&address="+encodeURIComponent(newRacerAddress.val())+
					"&city="+encodeURIComponent(newRacerCity.val())+"&state="+encodeURIComponent(newRacerState.val())+"&zip="+encodeURIComponent(newRacerZip.val())+
					"&phone="+encodeURIComponent(newRacerPhone.val())+"&email="+encodeURIComponent(newRacerEmail.val()),
					success: function(data) {
						$.mobile.changePage('/office/racers/', {transition:"slidedown", reloadPage:true});
					}
				});
			}
			return false;
		});
		
		//EDIT RACER
		$('button#editRacerBtn').click(function() {
			var editRacerFirstName = $('input[name=firstName]'),
				editRacerLastName = $('input[name=lastName]'),
				editRacerAddress = $('input[name=address]'),
				editRacerCity = $('input[name=city]'),
				editRacerState = $('input[name=state]'),
				editRacerZip = $('input[name=zipCode]'),
				editRacerPhone = $('input[name=phone]'),
				editRacerEmail = $('input[name=email]'),
				editRacerFlagged = $('select[name=flagged]'),
				editRID = $('input[name=rid]');
			
			if(editRacerFirstName.val() == "") {editRacerFirstName.css("background","red");} else {editRacerFirstName.css("background","white");};
			if(editRacerLastName.val() == "") {editRacerLastName.css("background","red");} else {editRacerLastName.css("background","white");};
			if(editRacerPhone.val() == "") {editRacerPhone.css("background","red");} else {editRacerPhone.css("background","white");};
			if(editRacerEmail.val() == "") {editRacerEmail.css("background","red");} else {editRacerEmail.css("background","white");};
			
			if(editRacerFirstName.val() != "" && editRacerLastName.val() != "" && editRacerPhone.val() != "" && editRacerEmail.val() != "") {
				$.ajax({
					type: "post",
					url: "/office/racers/editRacer.php",
					data: "fname="+encodeURIComponent(editRacerFirstName.val())+"&lname="+encodeURIComponent(editRacerLastName.val())+
					"&address="+encodeURIComponent(editRacerAddress.val())+"&city="+encodeURIComponent(editRacerCity.val())+"&state="+encodeURIComponent(editRacerState.val())+"&zip="+encodeURIComponent(editRacerZip.val())+
					"&phone="+encodeURIComponent(editRacerPhone.val())+"&email="+encodeURIComponent(editRacerEmail.val())+"&redflag="+editRacerFlagged.val()+"&rid="+editRID.val(),
					success: function(data) {
						$.mobile.changePage('/office/racers/', {transition:"slidedown", reloadPage:true});
					}
				});
			}
			return false;
		});
		
		//Edit Racer Notes
		$('a#editRacerNotes').click(function() {
			var historyID = $(this).attr("data-history"),
				textarea = $('textarea#racerNotes'+historyID),
				notes = encodeURIComponent(textarea.val());
			
			if(textarea.val() != "") {
				$.ajax({
					type: "post",
					url: "/office/racers/editRacerNotes.php",
					data: "h="+historyID+"&notes="+notes,
					success: function(data) {
						//$.mobile.changePage(window.location.href, {transition:"slidedown", reloadPage:true});
						$('div.ui-collapsible-set').find('.ui-collapsible').trigger('collapse');
					}
				});
			}
			
			return false;
		});
		
		//DELETE RACER
		$('a.deleteProfile').click(function() {
			if(confirm("Are you sure you want to delete this racers profile?")) {
				var profileID = $(this).attr("data-racerprofile");
				$.ajax({
					type: "post",
					url: "/office/global/callbacks/removeRacer.php",
					data: "r="+profileID,
					success: function(data) {
						var json = $.parseJSON(data);
						if(json.response == "success") {
							$.mobile.changePage('/office/racers/', {transition:"slidedown", reloadPage:true});
						} else {
							alert("Error in deleting profile. Please try again.");
						}
					}
				});
			}
			return false;
		});
		
		/*---------------------------------SUBMIT ALERTS---------------------------------*/
		$('form#addAlert button').click(function() {
			var alertField = $('textarea[name=alert]'),
				alertClass = $('input[name=class]'),
				classHash = $('input[name=hash]');
			
			if(alertField.val() == "") {alertField.css("background","red")} else {alertField.css("background","white")}

			$.ajax({
				type: "POST",
				url: "/office/class/alerts/new.php",
				data:"a="+encodeURIComponent(alertField.val())+"&c="+alertClass.val(),
				success: function(data) {
					$.mobile.changePage("/office/class/alerts/"+classHash.val(), {transition:"slidedown", reloadPage:true});
				}
			});
			
			return false;
		});
		
		/*---------------------------------RACER PACKAGES---------------------------------*/
		$('ul#racerPackages li a').click(function() {
			var racerID = $(this).attr("data-racer"),
				classID = $(this).attr("data-classhash");
			$.mobile.changePage("/office/class/packages/racerOptions.php?r="+racerID+"&c="+classID, {
				type: "POST",
				role: "dialog",
				transition: "slidedown"
			});
			return false;
		});
		
		$('input[name=agree]').click(function() {
			var elem = $('button#packageBtn');
			if($(this).is(':checked')) {
				elem.button('enable');
			} else {
				elem.button('disable');
			}
		});
		
		//SELECT PACKAGES
		$('ul#racerPackages input[type=checkbox]').bind("click",function() {
		
			if(this.id != "agree") {
				var packageSubTotal = 0,
					packageTax = 0;
					
				$('ul#racerPackages input[type=checkbox]').each(function() {
					var elem = $(this),
						elemAmount = elem.attr('data-price');
						
					if(elem.is(':checked')) {
						var taxRate = .0900;
						packageSubTotal += Number(elemAmount);
						if(elem.attr('data-taxable') == "true") {
							packageTax += Number(elemAmount) * Number(taxRate);
						}
					}
	
				});
				
				$('div#ui-packages-cart-stats span#packages-subtotal').html(packageSubTotal.toFixed(2));
				$('div#ui-packages-cart-stats span#packages-subtotal').prepend("$");
				$('input[name=packageSubTotal]').val(packageSubTotal.toFixed(2));
				$('div#ui-packages-cart-stats span#packages-tax').html(packageTax.toFixed(2));
				$('div#ui-packages-cart-stats span#packages-tax').prepend("$");
				$('input[name=packageTax]').val(packageTax.toFixed(2));
				$('div#ui-packages-cart-stats span#packages-total').html(Number(packageSubTotal + packageTax).toFixed(2));
				$('div#ui-packages-cart-stats span#packages-total').prepend("$");
				$('input[name=packageTotal]').val(Number(packageSubTotal + packageTax).toFixed(2));
			}
		});
		
		/*---------------------------------TRANSACTIONS---------------------------------*/
		$('select[name=transType]').change(function() {
			var transType = $(this).val();
			$.mobile.changePage("/office/transactions/"+transType+".php", {
				transition: "pop",
				role: "dialog"
			});
		});
		
		//FETCH RACER INFO
		$('select[name=racerProfile]').change(function() {
			var selectedRacer = $(this).val();
			$.ajax({
				type: 'post',
				url: '/office/global/callbacks/racerInfo.php',
				data: 'r='+selectedRacer,
				success: function(data) {
					var racerJSON = $.parseJSON(data);
					if(selectedRacer != "" && racerJSON.response == "success") {
						$('input[name=firstName]').val(racerJSON.firstName);
						$('input[name=lastName]').val(racerJSON.lastName);
						$('input[name=address]').val(racerJSON.address);
						$('input[name=city]').val(racerJSON.city);
						$('input[name=state]').val(racerJSON.state);
						$('input[name=zip]').val(racerJSON.zip);
						$('input[name=phone]').val(racerJSON.phone);
						$('input[name=email]').val(racerJSON.email);
						$('input[name=driverName]').val(racerJSON.firstName+" "+racerJSON.lastName);
						if(racerJSON.flagged == 1) {
							$('div#alertFlag a').attr('href','/office/racers/racerHistory.php?r='+racerJSON.id);
							$('div#alertFlag').show();
						} else {
							$('div#alertFlag').hide();		
						}
					} else {
						$('input[name=firstName]').val("");
						$('input[name=lastName]').val("");
						$('input[name=address]').val("");
						$('input[name=city]').val("");
						$('input[name=state]').val("");
						$('input[name=zip]').val("");
						$('input[name=phone]').val("");
						$('input[name=email]').val("");
						$('input[name=driverName]').val("");
					}
				}
			});
		});
		
		//FETCH PASS PRICE
		$('select#passType').change(function() {
			var passType = $(this).val();
			
			$.ajax({
				type: 'post',
				url: '/office/global/callbacks/passPrice.php',
				data: 'p='+passType,
				success: function(data) {
					var passJSON = $.parseJSON(data);
					if(passJSON != "" && passJSON.response == "success") {
						$('input[name=passPrice]').val(passJSON.price);
					} else {
						$('input[name=passPrice]').val("");
					}
				}
			});
			
		});
		
		//RACER PACKAGE TRANSACTION
		$('button#packageBtn').bind("click",function() {
			var insurance = ($('input[name=insurance]').prop('checked')) ? 1 : 0,
				youtube = ($('input[name=youtubeLink]').prop('checked')) ? 1 : 0,
				dvd = ($('input[name=dvdVideo]').prop('checked')) ? 1 : 0,
				bluray = ($('input[name=blueVideo]').prop('checked')) ? 1 : 0,
				photo = ($('input[name=photo]').prop('checked')) ? 1 : 0,
				plack = ($('input[name=plack]').prop('checked')) ? 1 : 0,
				cardNum = encodeURIComponent($('input[name=cardNum]').val()),
				cardExpM = encodeURIComponent($('select[name=cardExpM]').val()),
				cardExpY = encodeURIComponent($('select[name=cardExpY]').val()),
				cardCode = encodeURIComponent($('input[name=cardCode]').val()),
				tax = encodeURIComponent($('input[name=packageTax]').val()),
				total = encodeURIComponent($('input[name=packageSubTotal]').val()),
				classHash = encodeURIComponent($('input[name=classHash]').val());				
			
			//Ajax auth call
			$.ajax({
				type: 'POST',
				url: '/office/quickbooks/racerPackages.php',
				data: 'insurance='+insurance+'&youtube='+youtube+'&dvd='+dvd+'&bluray='+bluray+'&photo='+photo+'&plack='+plack+'&num='+cardNum+'&expM='+cardExpM+'&expY='+cardExpY+'&cvv='+cardCode+'&amount='+total+'&tax='+tax,
				success: function(data) {
					if(data == 1) {
						$('div.ui-corner-bottom.ui-content.ui-body-c').html('');
						$('div.ui-corner-bottom.ui-content.ui-body-c').html('<div style="position:relative; float:left; width:100%; text-align:center; color:black; font-size:20px; margin:50px 0;"><strong>Thank you for racer package purchase(s).</strong><br />Your order was successful.</div>');
						
						$.mobile.delay(3000).changePage("/office/class/packages/"+classHash, {transition:"slidedown", reloadPage:true});
					} else {
						$('ul#transForm li.cardErrors').html(data);
						$('ul#transForm li.cardErrors').show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						$('ul#transForm li.cardErrors').delay(5000).slideUp();
					}
				}
			});
			
			return false;
		});
		
		
		//CHARGE NEW RACE PASSES
		//NEW PASS
		$('button#newPassBtn').bind("click",function() {
			var firstName = encodeURIComponent($('input[name=firstName]').val()),
				lastName = encodeURIComponent($('input[name=lastName]').val()),
				address = encodeURIComponent($('input[name=address]').val()),
				city = encodeURIComponent($('input[name=city]').val()),
				state = encodeURIComponent($('input[name=state]').val()),
				zip = encodeURIComponent($('input[name=zip]').val()),
				phone = encodeURIComponent($('input[name=phone]').val()),
				email = encodeURIComponent($('input[name=email]').val()),
				racerProfile = encodeURIComponent($('select[name=racerProfile]').val()),
				passType = encodeURIComponent($('select[name=passType]').val()),
				driverName = encodeURIComponent($('input[name=driverName]').val()),
				passPrice = encodeURIComponent($('input[name=passPrice]').val()),
				cardNum = encodeURIComponent($('input[name=cardNum]').val()),
				cardExpM = encodeURIComponent($('select[name=cardExpM]').val()),
				cardExpY = encodeURIComponent($('select[name=cardExpY]').val()),
				cardCode = encodeURIComponent($('input[name=cardCode]').val());
			
			//Ajax auth call
			$.ajax({
				type: 'POST',
				url: '/office/quickbooks/auth.php',
				data: 'fname='+firstName+'&lname='+lastName+'&driver='+driverName+'&rid='+racerProfile+'&email='+email+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone+'&location=1&passNum=&passType='+passType+'&num='+cardNum+'&expM='+cardExpM+'&expY='+cardExpY+'&cvv='+cardCode+'&amount='+passPrice+'&tax=&notes=&type=newpass',
				success: function(data) {
					if(data == 1) {
						$.mobile.changePage("/office/racers/", {transition:"slidedown", reloadPage:true});
					} else {
						$('ul#transForm li.cardErrors').html(data);
						$('ul#transForm li.cardErrors').show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						$('ul#transForm li.cardErrors').delay(5000).slideUp();
					}
				}
			});
			
			return false;
		});
		
		//RE-RACE PASS
		$('button#reraceBtn').bind("click",function() {
			var firstName = encodeURIComponent($('input[name=firstName]').val()),
				lastName = encodeURIComponent($('input[name=lastName]').val()),
				address = encodeURIComponent($('input[name=address]').val()),
				city = encodeURIComponent($('input[name=city]').val()),
				state = encodeURIComponent($('input[name=state]').val()),
				zip = encodeURIComponent($('input[name=zip]').val()),
				phone = encodeURIComponent($('input[name=phone]').val()),
				email = encodeURIComponent($('input[name=email]').val()),
				racerProfile = encodeURIComponent($('select[name=racerProfile]').val()),
				passType = encodeURIComponent($('select[name=passType]').val()),
				driverName = encodeURIComponent($('input[name=driverName]').val()),
				passPrice = encodeURIComponent($('input[name=passPrice]').val()),
				cardNum = encodeURIComponent($('input[name=cardNum]').val()),
				cardExpM = encodeURIComponent($('select[name=cardExpM]').val()),
				cardExpY = encodeURIComponent($('select[name=cardExpY]').val()),
				cardCode = encodeURIComponent($('input[name=cardCode]').val());
			
			//Ajax auth call
			$.ajax({
				type: 'POST',
				url: '/office/quickbooks/auth.php',
				data: 'fname='+firstName+'&lname='+lastName+'&driver='+driverName+'&rid='+racerProfile+'&email='+email+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone+'&location=1&passNum=&passType='+passType+'&num='+cardNum+'&expM='+cardExpM+'&expY='+cardExpY+'&cvv='+cardCode+'&amount='+passPrice+'&tax=&notes=&type=rerace',
				success: function(data) {
					if(data == 1) {
						$.mobile.changePage("/office/racers/", {transition:"slidedown", reloadPage:true});
					} else {
						$('ul#transForm li.cardErrors').html(data);
						$('ul#transForm li.cardErrors').show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						$('ul#transForm li.cardErrors').delay(5000).slideUp();
					}
				}
			});
			
			return false;
		});
		
		//SCHEDULE CHARGE
		$('button#scheduleFeeBtn').bind("click",function() {
			var firstName = encodeURIComponent($('input[name=firstName]').val()),
				lastName = encodeURIComponent($('input[name=lastName]').val()),
				address = encodeURIComponent($('input[name=address]').val()),
				city = encodeURIComponent($('input[name=city]').val()),
				state = encodeURIComponent($('input[name=state]').val()),
				zip = encodeURIComponent($('input[name=zip]').val()),
				phone = encodeURIComponent($('input[name=phone]').val()),
				email = encodeURIComponent($('input[name=email]').val()),
				racerProfile = encodeURIComponent($('select[name=racerProfile]').val()),
				feePrice = encodeURIComponent($('input[name=feePrice]').val()),
				cardNum = encodeURIComponent($('input[name=cardNum]').val()),
				cardExpM = encodeURIComponent($('select[name=cardExpM]').val()),
				cardExpY = encodeURIComponent($('select[name=cardExpY]').val()),
				cardCode = encodeURIComponent($('input[name=cardCode]').val()),
				notes = encodeURIComponent($('textarea[name=feeNotes]').val());
			
			//Ajax auth call
			$.ajax({
				type: 'POST',
				url: '/office/quickbooks/auth.php',
				data: 'fname='+firstName+'&lname='+lastName+'&driver=&rid='+racerProfile+'&email='+email+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone+'&location=1&passNum=&passType=&num='+cardNum+'&expM='+cardExpM+'&expY='+cardExpY+'&cvv='+cardCode+'&amount='+feePrice+'&tax=&notes='+notes+'&type=schedule',
				success: function(data) {
					if(data == 1) {
						$.mobile.changePage("/office/racers/", {transition:"slidedown", reloadPage:true});
					} else {
						$('ul#transForm li.cardErrors').html(data);
						$('ul#transForm li.cardErrors').show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						$('ul#transForm li.cardErrors').delay(5000).slideUp();
					}
				}
			});
			
			return false;
		});
		
		//REFUND PASS
		$('button#refundBtn').bind("click",function() {
			var firstName = encodeURIComponent($('input[name=firstName]').val()),
				lastName = encodeURIComponent($('input[name=lastName]').val()),
				address = encodeURIComponent($('input[name=address]').val()),
				city = encodeURIComponent($('input[name=city]').val()),
				state = encodeURIComponent($('input[name=state]').val()),
				zip = encodeURIComponent($('input[name=zip]').val()),
				phone = encodeURIComponent($('input[name=phone]').val()),
				email = encodeURIComponent($('input[name=email]').val()),
				racerProfile = encodeURIComponent($('select[name=racerProfile]').val()),
				refundAmount = encodeURIComponent($('input[name=refundAmount]').val()),
				passNum = encodeURIComponent($('input[name=passNum]').val()),
				cardNum = encodeURIComponent($('input[name=cardNum]').val()),
				cardExpM = encodeURIComponent($('select[name=cardExpM]').val()),
				cardExpY = encodeURIComponent($('select[name=cardExpY]').val()),
				cardCode = encodeURIComponent($('input[name=cardCode]').val()),
				notes = encodeURIComponent($('textarea[name=refundNotes]').val());
			
			//Ajax auth call
			$.ajax({
				type: 'POST',
				url: '/office/quickbooks/auth.php',
				data: 'fname='+firstName+'&lname='+lastName+'&driver=&rid='+racerProfile+'&email='+email+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone+'&location=&passNum='+passNum+'&passType=&num='+cardNum+'&expM='+cardExpM+'&expY='+cardExpY+'&cvv='+cardCode+'&amount='+refundAmount+'&tax=&notes='+notes+'&type=refund',
				success: function(data) {
					if(data == 1) {
						$.mobile.changePage("/office/racers/", {transition:"slidedown", reloadPage:true});
					} else {
						$('ul#transForm li.cardErrors').html(data);
						$('ul#transForm li.cardErrors').show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						$('ul#transForm li.cardErrors').delay(5000).slideUp();
					}
				}
			});
			
			return false;
		});
		
		/*---------------------------------INSURANCE---------------------------------*/
		$('button#clearSig').click(function() {
			var canvas = document.getElementById("canvas"),
				ctx = canvas.getContext("2d"),
				currentSigHeight = $('canvas.jSignature').height();
			
			ctx.clearRect(0, 0, "100%", currentSigHeight);
			return false;
		});
		
		/*---------------------------------RACE PASSES---------------------------------*/
		$('a#processPassBtn').click(function() {
			var processPassHash = $(this).attr('data-pass-hash');
			//alert(processPassHash);
			$.ajax({
				type: 'post',
				url: '/office/global/callbacks/processPass.php',
				data: 'p='+processPassHash,
				success: function(data) {
					//$('ul').listview('refresh')
					$.mobile.changePage(window.location.href, { allowSamePageTransition: true, transition: 'none', reloadPage: true });
				}
			});
			return false;
		});
		
		//EDIT PASS
		$('button#editPassBtn').click(function() {
			var passBuyerName = $('input[name=buyerName]'),
				passDriverName = $('input[name=driverName]'),
				passTracks = $('select[name=tracks]'),
				passVideo = $('select[name=video]'),
				passType = $('select[name=passType]'),
				passStatus = $('select[name=passStatus]'),
				passClass = $('select[name=class]'),
				passProfile = $('select[name=profile]'),
				passPID = $('input[name=pid]');
				
			//UPDATE PASS
			$.ajax({
				type: 'post',
				url: '/office/passes/viewPass.php',
				data: 'bn='+encodeURIComponent(passBuyerName.val())+"&dn="+encodeURIComponent(passDriverName.val())+"&tracks="+encodeURIComponent(passTracks.val())+"&video="+encodeURIComponent(passVideo.val())+
				"&type="+encodeURIComponent(passType.val())+"&status="+encodeURIComponent(passStatus.val())+"&class="+encodeURIComponent(passClass.val())+"&profile="+encodeURIComponent(passProfile.val())+
				"&pid="+encodeURIComponent(passPID.val()),
				success: function(data) {
					$.mobile.changePage("/office/passes/", {transition:"slidedown", reloadPage:true});
				}
			});
				
			return false;
		});
		
		
		/*-----------------------------------CHECK IN----------------------------------*/
		$('button#checkInBtn').click(function() {
			var checkInRacerID = $(this).attr("data-racerid"),
				checkInClass = $(this).attr("data-classhash");
			
			$.ajax({
				type: "post",
				url: "/office/global/callbacks/checkin.php",
				data: "c="+checkInClass+"&r="+checkInRacerID,
				success: function(data) {
					var checkInJSON = $.parseJSON(data);
					if(checkInJSON.response == "success") {
						$('ul#racerCheckIn').html('<div style="position:relative; float:left; width:100%; text-align:center; color:black; font-size:20px; margin:50px 0;"><strong>Thank you for checking in.</strong><br />Please move to station 2.</div>');
						setTimeout(function() {
							$.mobile.changePage("/office/class/"+checkInClass, {transition:"slidedown", reloadPage:true});
						}, 3000);
					} else {
						$('ul#racerCheckIn').html('<div style="position:relative; float:left; width:100%; text-align:center; color:black; font-size:20px; margin:50px 0;">'+checkInJSON.response+'</div>');
					}
				}
			});
			
			return false;
		});
		
		/*---------------------------------ASSIGN CARS---------------------------------*/
		$('select[name=assignCar]').change(function() {
			var assignID = this.id,
				assignedCar = $(this).val(),
				assignedRacer = $(this).attr("data-racerid"),
				assignedClass = $(this).attr("data-classid");
			
			$.ajax({
				type: 'POST',
				url: '/office/global/callbacks/assignCar.php',
				data: 'c='+assignedClass+"&r="+assignedRacer+"&car="+assignedCar,
				success: function(data) {
					var assignData = $.parseJSON(data);
					if(assignData.response === "noncamera") {
						alert("The car you selected doesn't have an active camera. Please choose a different car.");
					}
				}
			});
			
		});
		
		/*---------------------------------ASSIGN RACER TO CLASS ---------------------------------*/
		$('a#assignRacerBtn').click(function() {
			var racerHash = $(this).attr('data-assign-racer'),
				classHash = $(this).attr('data-class-hash');
			
			if(confirm("Add this racer to the class?")) {
				$.ajax({
					type: 'post',
					url: '/office/global/callbacks/assignRacer.php',
					data: 'c='+classHash+'&r='+racerHash,
					success: function(data) {
						$.mobile.changePage(window.location.href, { allowSamePageTransition: true, transition: 'none', reloadPage: true });
					}
				});
			}
			return false;
		});
		
		
		/*---------------------------------REMOVE ASSIGNED RACER FROM CLASS ---------------------------------*/
		$('a#removeAssignedRacerBtn').click(function() {
			var racerHash = $(this).attr('data-assign-racer'),
				classHash = $(this).attr('data-class-hash');
			
			if(confirm("Remove this racer from the class?")) {
				$.ajax({
					type: 'post',
					url: '/office/global/callbacks/removeAssignedRacer.php',
					data: 'c='+classHash+'&r='+racerHash,
					success: function(data) {
						$.mobile.changePage(window.location.href, { allowSamePageTransition: true, transition: 'none', reloadPage: true });
					}
				});
			}
			return false;
		});

	});
	
	
	
	
/*!
 * jQuery Cookie Plugin v1.3
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function ($, document, undefined) {

	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	var config = $.cookie = function (key, value, options) {

		// write
		if (value !== undefined) {
			options = $.extend({}, config.defaults, options);

			if (value === null) {
				options.expires = -1;
			}

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = config.json ? JSON.stringify(value) : String(value);

			return (document.cookie = [
				encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// read
		var decode = config.raw ? raw : decoded;
		var cookies = document.cookie.split('; ');
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			if (decode(parts.shift()) === key) {
				var cookie = decode(parts.join('='));
				return config.json ? JSON.parse(cookie) : cookie;
			}
		}

		return null;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) !== null) {
			$.cookie(key, null, options);
			return true;
		}
		return false;
	};

})(jQuery, document);