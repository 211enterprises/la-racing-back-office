<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c'");
		$class = $classQuery->fetch_assoc();
		$classID = $class['id'];
			
	}
	
	if(isset($_POST['a']) && isset($_POST['c'])) {
		$alert = makeSQLSafe($mysqli,$_POST['a']);
		$classAlert = makeSQLSafe($mysqli,$_POST['c']);
		$datetime = date("Y-m-d H:i:s");
		
		if($_POST['a'] != "" && $_POST['c'] != "") {
			$mysqli->query("INSERT INTO `LARX_class_alerts` (`class_id`,`alert`,`timestamp`) VALUES ('$classAlert','$alert','$datetime')");
		}
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Add Class Alert</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($classQuery->num_rows == 1) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1>Add Class Alert</h1>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php if($classQuery->num_rows > 0) { ?>
	
	<form action="" method="post" id="addAlert">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<textarea name="alert" id="alert" placeholder="Write an alert..."></textarea>
			</li>
			<li data-role="fieldcontain">
				<button type="submit" data-theme="a" data-icon="arrow-r" data-iconpos="right">Add Alert</button>
			</li>
			<input type="hidden" name="class" id="class" value="<?php echo $classID; ?>" />
			<input type="hidden" name="hash" id="hash" value="<?php echo $class['class_hash']; ?>" />
		</ul>
	</form>
	
<?php } else echo '<h1 style="text-align:center;">No Class Date Found.</h1>'; ?>
   		
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>