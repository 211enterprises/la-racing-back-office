<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c'");
		$class = $classQuery->fetch_assoc();
		$classID = $class['id'];
		
		//FETCH ALERTS
		$alertQuery = $mysqli->query("SELECT * FROM `LARX_class_alerts` WHERE `class_id` = '$classID' ORDER BY `timestamp` DESC");
		
	} else {
		header("Location: /office/scheduling/");
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>Class Alerts: <?php echo date("m/d/Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="classAlerts">
   
   <!--HEADER-->
   <div data-role="header">
	   <a href="/office/schedule/" data-icon="home" data-iconpos="left" data-transition="slide">Home</a>
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
	   <a href="/office/class/alerts/new.php?c=<?php echo $c; ?>" data-role="button" class="ui-btn-right" data-rel="dialog" data-icon="plus" data-iconpos="left" data-transition="slidedown">Add Alert</a>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($alertQuery->num_rows > 0) { ?>
   		<ul data-role="listview" data-icon="arrow-d">
   	<?php while($alert = $alertQuery->fetch_array()) { ?>
   			<li>
				<a href="">
					<p class="ui-li-aside"><strong><?php echo date("h:i A",strtotime($alert['timestamp'])); ?></strong></p>
					<h3></h3>
					<p class="ui-li-desc">
						<?php echo substr($alert['alert'],0,200); ?>
					</p>
				</a>
			</li>
	<?php } ?>
   		</ul>
<?php } else { ?>
		<h3></h3>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo classFooter("alerts",$c); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>