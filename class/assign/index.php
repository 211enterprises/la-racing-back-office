<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c'");
		$class = $classQuery->fetch_assoc();
		$classID = $class['id'];
		$classMax = $class['class_limit'] / 4;
		
		//FETCH GROUPING
		$groupQuery = $mysqli->query("SELECT * FROM `LARX_class_grouping` WHERE `class_id` = '$classID' LIMIT 1");
		$group = $groupQuery->fetch_assoc();
		
	} else {
		header("Location: /office/scheduling/");
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>Assign Racers: <?php echo date("m/d/Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link type="text/css" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" rel="stylesheet" />
<link type="text/css" href="/office/global/style/custom.css" rel="stylesheet" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="classAssign">
   
   <!--HEADER-->
   <div data-role="header">
	   <a href="/office/schedule/" data-icon="home" data-iconpos="left" data-transition="slide">Home</a>
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
  
<?php for($g = 1; $g <= $classMax; $g++) { ?>
	    <ul data-role="listview" class="classAssignCars" id="group_<?php echo $g; ?>" data-inset="true">
		   <li data-listdivider="true" data-theme="a" data-skipcount="true" class="ui-state-disabled">Group #<?php echo $g; ?>:</li>
<?php
	$groupData = explode(",",str_replace(array('[',']'),'',$group['group_data'])); 
	$groupArrays = array();
	foreach($groupData as $value) {
		$groupArray = explode("=>",$value);
		if(str_replace("group_","",$groupArray[0]) == $g) {
			$assignRacer = $groupArray[1];
			//FETCH ROSTER
			$rosterQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$assignRacer' LIMIT 1");
			$roster = $rosterQuery->fetch_assoc();
			$racerID = $roster['id'];
			//FETCH AVAILABLE CARS
			$carQuery = $mysqli->query("SELECT * FROM `LARX_race_cars` WHERE `active` = 1 ORDER BY `car_number` ASC");
			//FETCH ASSIGNED CARS
			$assignQuery = $mysqli->query("SELECT * FROM `LARX_class_assign` WHERE `class_id` = '$classID' AND `racer_id` = '$racerID' LIMIT 1");
			$assign = $assignQuery->fetch_array(); ?>
			<li>
   				<?php if($roster['driver_name'] != NULL) echo $roster['driver_name']; else echo $roster['buyer_name']; ?>
   				<label for="assignCar<?php echo $roster['id']; ?>"></label>
   				<select name="assignCar" data-classid="<?php echo $classID; ?>" data-racerid="<?php echo $roster['id']; ?>" id="assignCar<?php echo $roster['id']; ?>">
   					<option value="">Select Car:</option>
   			<?php while($car = $carQuery->fetch_array()) { ?>
   					<option value="<?php echo $car['id']; ?>" <?php if($car['id'] == $assign['car_id']) echo 'selected="selected"'; ?>><?php echo $car['car_number']; ?></option>
   			<?php } ?>
   				</select>
   			</li>
<?php
		}
	}
?>
		</ul>
	<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo classFooter("assign",$c); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$rosterQuery->close();
$mysqli->close();	
?>