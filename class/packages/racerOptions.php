<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$r = makeSQLSafe($mysqli,$_GET['r']);
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['r'] != "" && $_GET['c'] != "") {
		//QUERY RACER
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$r' LIMIT 1");
		$racer = $racerQuery->fetch_assoc();
		//QUERY CLASS
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		$class = $classQuery->fetch_assoc();
			
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Racer Options</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($classQuery->num_rows == 1) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php if($racer['driver_name'] != "") echo $racer['driver_name']; else echo $racer['buyer_name']; echo " - Racer Options"; ?></h1>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php
if($classQuery->num_rows == 1) {
	if($racerQuery->num_rows == 1) { ?>
	
		<form action="" method="post">
			<ul data-role="listview" id="racerPackages">
				<li data-listdivider="true" data-theme="b">Choose your packages:</li>
				<li data-role="fieldcontain" class="cardErrors">
					<div class="ui-grid-solo"></div>
				</li>
				<!--INSURANCE-->
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<strong>Insurance Coverage</strong>
						<div class="optionPrice">$89</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
						</p>
						<label for="insurance">Apply</label>
						<input type="checkbox" name="insurance" id="insurance" data-price="89.00" data-taxable="false" />
					</div>
				</li>
				<!--IN CAR CAMERA-->
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<strong>In Car Footage</strong>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
						</p>
						<label for="youtubeLink">YouTube Link - $69.00</label>
						<input type="checkbox" name="youtubeLink" id="youtubeLink" data-price="69.00" data-taxable="true" />
						<label for="dvdVideo">DVD - $99.00</label>
						<input type="checkbox" name="dvdVideo" id="dvdVideo" data-price="99.00" data-taxable="true" />
						<label for="blueVideo">Blu-Ray - $99.00</label>
						<input type="checkbox" name="blueVideo" id="blueVideo" data-price="99.00" data-taxable="true" />
					</div>
			<?php if($racer['race_video'] == 1) { ?>
					<div class="packageIncludedWrap">
						<div class="includeBG"></div>
						<h4>In Car Footage Included</h4>
					</div>
			<?php } ?>
				</li>
				<!--RACING PHOTO-->
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<strong>In Car Photo</strong>
						<div class="optionPrice">$19.99<br /><span>plus tax</span></div>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
						</p>
						<label for="photo">Apply</label>
						<input type="checkbox" name="photo" id="photo" data-price="19.99" data-taxable="true" />
					</div>
				</li>
				<!--RACING PLACK-->
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<strong>LA Racing Plaque</strong>
						<div class="optionPrice">$69.99<br /><span>plus tax</span></div>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
						</p>
						<label for="plack">Apply</label>
						<input type="checkbox" name="plack" id="plack" data-price="69.99" data-taxable="true" />
					</div>
				</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-a">
						<div class="ui-block-a"></div>
						<div class="ui-block-b" id="ui-packages-cart-stats">
							Subtotal: <span id="packages-subtotal">$0.00</span><br />
							Tax: <span id="packages-tax">$0.00</span><br />
							Packages Total:<br />
							<span id="packages-total">$0.00</span>
						</div>
					</div>
				</li>
				<li data-listdivider="true" data-theme="a">Credit Card Information:</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-a">
						<div class="ui-block-a">
							<label for="cardNum">Card Number:</label><br />
							<input type="text" name="cardNum" id="cardNum" />
						</div>
						<div class="ui-block-b">
							<label for="cardExpM">Card Expiration:</label><br />
							<fieldset data-role="controlgroup" id="cardExp" data-type="horizontal" style="width:240px;">
								<select name="cardExpM" id="cardExpM">
									<optgroup label="Expiration Month:">
										<option value="">Mon:</option>
								<?php for($m = 01; $m <= 12; $m++) { ?>
										<option value="<?php echo $m; ?>"><?php echo $m; ?></option>
								<?php } ?>
									</optgroup>
								</select>
								<select name="cardExpY" id="cardExpY">
									<optgroup label="Expiration Year:">
										<option value="">Year:</option>
										<option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
										<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
										<option value="<?php echo date("Y") + 2; ?>"><?php echo date("Y") + 2; ?></option>
										<option value="<?php echo date("Y") + 3; ?>"><?php echo date("Y") + 3; ?></option>
										<option value="<?php echo date("Y") + 4; ?>"><?php echo date("Y") + 4; ?></option>
										<option value="<?php echo date("Y") + 5; ?>"><?php echo date("Y") + 5; ?></option>
									</optgroup>
								</select>
							</fieldset>
						</div>
					</div>
				</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-a">
						<div class="ui-block-a">
							<label for="cardCode">Card Security Code:</label><br />
							<input type="text" name="cardCode" id="cardCode" />
						</div>
						<div class="ui-block-b"></div>
					</div>
				</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-a">
						<div class="ui-block-a">
							<label for="agree"> I agree to all releases/policies</label>
							<input type="checkbox" name="agree" id="agree" data-price="0.00" />
						</div>
						<div class="ui-block-b" style="text-align:right;">
							<button type="submit" data-theme="a" id="packageBtn" disabled="disabled">Submit</button>
						</div>
					</div>
				</li>
			</ul>
			<input type="hidden" name="classHash" value="<?php echo $c; ?>" />
			<input type="hidden" name="packageSubTotal" value="0.00" />
			<input type="hidden" name="packageTax" value="0.00" />
			<input type="hidden" name="packageTotal" value="0.00" />
		</form>
	
<?php } else echo '<h3 style="text-align:center;">Racer Not Found.</h3>';
} else echo '<h3 style="text-align:center;">Racer Not Found.</h3>'; ?>
 	
	</div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$racerQuery->close();
$classQuery->close();
$mysqli->close();	
?>