<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$r = makeSQLSafe($mysqli,$_GET['r']);
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['r'] != "" && $_GET['c'] != "") {
		//QUERY RACER
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` WHERE `id` = '$r'");
		$racer = $racerQuery->fetch_assoc();
		//QUERY CLASS
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c'");
		$class = $classQuery->fetch_assoc();
			
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Insurance Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<!--[if lt IE 9]><script type="text/javascript" src="/office/global/js/flashcanvas.js"></script><![endif]-->
<script src="/office/global/js/jSignature.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script>
	$(document).ready(function() {
		$("#signature").jSignature()
    });
</script>
</head>
<body>

<div data-role="page">
   
<?php if($classQuery->num_rows == 1) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo $racer['first_name']." ".$racer['last_name']." - Insurance Form"; ?></h1>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php
if($classQuery->num_rows == 1) {
	if($racerQuery->num_rows == 1) { ?>
	
		<form action="" method="post">
			<ul data-role="listview" id="insuranceForm">
				<li data-listdivider="true" data-theme="b">LA Racing Insurance:</li>
				<li data-role="fieldcontain">
					
				</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<div id="signature"></div>
					</div>
				</li>
				<li data-role="controlgroup" data-type="horizontal">
					<div style="width:100%; text-align:center;">
						<button type="button" id="clearSig">Clear Signature</button>
						<button type="submit">I Agree</button>
					</div>
				</li>
			</ul>
		</form>
	
<?php } else echo '<h3 style="text-align:center;">Racer Not Found.</h3>';
} else echo '<h3 style="text-align:center;">Class Not Found.</h3>'; ?>
 	
	</div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$racerQuery->close();
$classQuery->close();
$mysqli->close();	
?>