<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		if($classQuery->num_rows == 1) {
			$class = $classQuery->fetch_assoc();
			$classID = $class['id'];
		
			//FETCH ROSTER
			$rosterQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `class_id` = '$classID' AND `pass_status` = 'valid' OR `class_id` = '$classID' AND `pass_status` = 'used' ORDER BY `timestamp` DESC");
			
		} else {
			header("Location: /office/scheduling/");
			exit;
		}
	} else {
		header("Location: /office/scheduling/");
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing Class: <?php echo date("m/d/Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main" data-title="LA Racing X | Upcoming Class Dates">
   
   <!--HEADER-->
   <div data-role="header">
	   <a href="/office/schedule/" data-icon="home" data-iconpos="left" data-transition="slide">Home</a>   	
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
   		<ul data-role="listview" data-inset="true" data-icon="arrow-d">
			<li data-role="list-divier" data-theme="a">Click on your name to check in:</li>
	<?php while($roster = $rosterQuery->fetch_array()) { ?>
			<li>
				<a href="racerCheckIn.php?r=<?php echo $roster['id']; ?>&c=<?php echo $class['class_hash']; ?>" data-rel="dialog"><?php if($roster['driver_name'] != NULL) echo $roster['driver_name']; else echo $roster['buyer_name']; ?></a>
				<?php if($roster['checked_in'] == 1) echo '<div class="racer-checkin"><img src="/office/global/images/checkInIcon.png" alt="Racer Checked In" /></div>'; ?>
			</li>
	<?php } ?>
		</ul>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo classFooter("checkin",$c); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>