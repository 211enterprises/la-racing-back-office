<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$r = makeSQLSafe($mysqli,$_GET['r']);
	$c = makeSQLSafe($mysqli,$_GET['c']);
	
	if(isset($_GET['r']) && isset($_GET['c'])) {
		//QUERY CLASS
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		$class = $classQuery->fetch_assoc();
		//QUERY RACER
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$r' AND `pass_status` IN ('valid','used') LIMIT 1");
		$racer = $racerQuery->fetch_assoc();
			
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Racer Check In</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($classQuery->num_rows == 1) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php if($racer['driver_name'] != "") echo $racer['driver_name']; else echo $racer['buyer_name']; ?></h1>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php
if($classQuery->num_rows == 1) {
	if($racerQuery->num_rows == 1) { ?>
	
		<form action="" method="post">
			<ul data-role="listview" id="racerCheckIn">
				<li data-listdivider="true" data-theme="b">Racer Check In:</li>
				<!--INSURANCE-->
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<strong><?php echo date("m-d-Y - h:i A"); ?></strong>
						<p>
							Are you ready to race? Click the button below to check yourself in. Please sign the appropriate release forms provided, and feel free to move to station 2.
						</p>
					</div>
				</li>
				<li data-role="fieldcontain">
					<div class="ui-grid-solo">
						<button type="submit" data-theme="a" data-racerid="<?php echo $racer['id']; ?>" data-classhash="<?php echo $class['class_hash']; ?>" id="checkInBtn">Check In</button>
					</div>
				</li>
			</ul>
		</form>
	
<?php } else echo '<h3 style="text-align:center;">Racer Not Found.</h3>';
} else echo '<h3 style="text-align:center;">Class Not Found.</h3>'; ?>
 	
	</div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$racerQuery->close();
$classQuery->close();
$mysqli->close();	
?>