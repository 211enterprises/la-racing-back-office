<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		$class = $classQuery->fetch_assoc();
		$classID = $class['id'];
		$classHash = $class['class_hash'];
		$classLimit = $class['class_limit'];
		$groupNum = ceil($classLimit / 4);
		
		//FETCH ROSTER
		$rosterQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `class_id` = '$classID' AND `pass_status` = 'valid' ORDER BY `timestamp` DESC");
		
		//FETCH GROUPING
		$groupQuery = $mysqli->query("SELECT * FROM `LARX_class_grouping` WHERE `class_id` = '$classID' LIMIT 1");
		
	} else {
		header("Location: /office/scheduling/");
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>Class Grouping: <?php echo date("m/d/Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="image/png" href="/media/images/favicon.ico" rel="icon" />
<link type="text/css" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" rel="stylesheet" />
<link type="text/css" href="/office/global/style/custom.css" rel="stylesheet" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="https://raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript">
  $(document).bind('pageinit', function() {
    $('div.ui-grid-a#classGrouping ul li').removeClass('ui-corner-top');
    $('div.ui-grid-a#classGrouping ul li').removeClass('ui-corner-bottom');
    $('div.ui-grid-a#classGrouping ul li').addClass('ui-corner-all');
    $('div.ui-grid-a#classGrouping ul').sortable({
	        connectWith: 'div.ui-grid-a#classGrouping ul',
	        placeholder: "ui-state-highlight",
	        items: "li:not(.ui-state-disabled)",
	        stop: function(event, ui) {
		        var groupData = new Array();
	            $('div.ui-grid-a ul li,div.ui-grid-b ul li').each(function() {
	            	var elem = $(this);
	            	if(elem.attr("data-skipcount") !== "true") {
	            		groupData += Array(elem.parent().attr("id")+"-"+elem.attr("data-racer")+",");
	            	}
	            });
            	var classHash = $('ul#ungrouped').attr("data-class-hash");
            	$.ajax({
	            	type: 'post',
	            	url: '/office/global/callbacks/classGrouping.php',
	            	data: 'c='+classHash+'&d='+encodeURIComponent(groupData),
	            	error: function() {
	            		alert('Grouping Error. Try again!');
	            	}
            	});
	        }
	  });
	  $('div.ui-grid-a#classGrouping ul li').disableSelection();
	  
	  (function($) {
		    $.fn.serial = function() {
		        var array = [];
		        var $elem = $(this);
		        $elem.each(function(i) {
		            var menu = this.id;
		            $('li', this).each(function(e) {
		                array.push(menu + '[' + e + ']=' + $(this).attr('data-racer'));
		            });
		        });
		        return array.join('&');
		    }
	   })(jQuery);
	  
  });
</script>
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page">
   
   <!--HEADER-->
   <div data-role="header">
	   <a href="/office/schedule/" data-icon="home" data-iconpos="left" data-transition="slide">Home</a>
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
	   <div class="ui-grid-a" id="classGrouping">
		   <div class="ui-block-a">
			   <!--CLASS ROSTER-->
			   <ul data-role="listview" id="ungrouped" data-inset="true" data-class-hash="<?php echo $classHash; ?>">
				   <li data-listdivider="true" data-theme="a" data-skipcount="true" class="ui-state-disabled">Unassigned Racers:</li>
	  <?php if($groupQuery->num_rows == 1) {
					echo classGrouping($mysqli,$classID,"ungrouped");
				} else {
				while($racer = $rosterQuery->fetch_array()) { ?>
					<li data-racer="<?php echo $racer['id']; ?>"><?php if($racer['driver_name'] != NULL) echo $racer['driver_name']; else echo $racer['buyer_name']; ?></li>
		  <?php }
			} ?>
			   </ul>
		   </div>
		   <div class="ui-block-b">
	<?php for($g = 1; $g <= $groupNum; $g++) { ?>
			   <ul data-role="listview" id="group_<?php echo $g; ?>" data-inset="true">
				   <li data-listdivider="true" data-theme="a" data-skipcount="true" class="ui-state-disabled">Group #<?php echo $g; ?>:</li>
				   <?php echo classGrouping($mysqli,$classID,"group_".$g); ?>
			   </ul>
	<?php } ?>
		   </div>
	   </div>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo classFooter("grouping",$c); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$rosterQuery->close();
$mysqli->close();	
?>