<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	if(isset($_POST['fname']) && isset($_POST['lname'])) {
		$firstName = makeSQLSafe($mysqli,$_POST['fname']);
		$lastName = makeSQLSafe($mysqli,$_POST['lname']);
		$address = makeSQLSafe($mysqli,$_POST['address']);
		$city = makeSQLSafe($mysqli,$_POST['city']);
		$state = makeSQLSafe($mysqli,$_POST['state']);
		$zipCode = makeSQLSafe($mysqli,$_POST['zip']);
		$phone = makeSQLSafe($mysqli,$_POST['phone']);
		$email = makeSQLSafe($mysqli,$_POST['email']);
		
		//INSERT NEW RACER
		$mysqli->query("INSERT INTO `LARX_racer_profiles` (`first_name`,`last_name`,`address`,`city`,`state`,`zip_code`,`phone`,`email_address`,`timestamp`)
		VALUES ('$firstName','$lastName','$address','$city','$state','$zipCode','$phone','$email',NOW())");
		
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Add Racer Profile</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page" id="addRacerDialog">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1>Add Racer Profile</h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
	
	<form action="" method="post">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<label for="firstName">First Name:</label>
				<input type="text" name="firstName" id="firstName" />
			</li>
			<li data-role="fieldcontain">
				<label for="lastName">Last Name:</label>
				<input type="text" name="lastName" id="lastName" />
			</li>
			<li data-role="fieldcontain">
				<label for="address">Address:</label>
				<input type="text" name="address" id="address" />
			</li>
			<li data-role="fieldcontain">
				<label for="city">City:</label>
				<input type="text" name="city" id="city" />
			</li>
			<li data-role="fieldcontain">
				<label for="state">State:</label>
				<input type="text" name="state" id="state" />
			</li>
			<li data-role="fieldcontain">
				<label for="zipCode">Zip Code:</label>
				<input type="text" name="zipCode" id="zipCode" />
			</li>
			<li data-role="fieldcontain">
				<label for="phone">Phone #:</label>
				<input type="tel" name="phone" id="phone" />
			</li>
			<li data-role="fieldcontain">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" />
			</li>
			<li data-role="fieldcontain">
				<button type="submit" id="addRacerBtn" data-theme="a" data-icon="arrow-r" data-iconpos="right">Add Racer</button>
			</li>
		</ul>
		<input type="hidden" name="addRacer" />
	</form>
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$mysqli->close();	
?>