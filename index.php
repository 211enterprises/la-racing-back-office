<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	//$password = SHA1("admin".SHA1("laracingx"));
	//$mysqli->query("INSERT INTO `LARX_office_users` (`username`,`password`) VALUES ('admin','$password')");

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/office/global/images/icon-apple-touch.png" rel="apple-touch-icon" />
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
   		<form action="/office/schedule/" method="post">
   			<div data-role="controlgroup" class="ui-login">
   				<div class="loginError"></div>
   				<fieldset>
	   				<label for="username">Username:</label>
	   				<input type="text" name="username" id="username" />
	   				<label for="password">Password:</label>
	   				<input type="password" name="password" id="password" />
	   				<button data-theme="a" type="submit" id="loginBtn" data-corners="true" data-icon="arrow-r" data-iconpos="right">Login</button>
   				</fieldset>
   			</div>
   		</form>
   		
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>

