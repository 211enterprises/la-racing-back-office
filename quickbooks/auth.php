<?php

require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");

/**
 * Example of connecting PHP to the QuickBooks Merchant Service
 * @package QuickBooks
 * @subpackage Documentation
 */

// Show errors
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', true);

// Plain text output
header('Content-Type: text/plain');

// Include the QuickBooks files
require_once 'QuickBooks.php';

// If you want to log requests/responses to a database, you can provide a 
//	database DSN-style connection string here
$dsn = null;
// $dsn = 'mysql://root:@localhost/quickbooks_merchantservice';

// If you're using the DESKTOP model
$path_to_private_key_and_certificate = null;

// This is your login ID that Intuit assignes you during the application 
//	registration process.
$application_login = 'office.laracingx.com';
$connection_ticket = 'SDK-TGT-180-Ipv8VDWzYcYBIEbeqqBWNg';

// Create an instance of the MerchantService object 
$MS = new QuickBooks_MerchantService(
	$dsn, 
	$path_to_private_key_and_certificate, 
	$application_login,
	$connection_ticket);

// If you're using a Intuit QBMS development account, you must set this to true! 
$MS->useTestEnvironment(false);

// If you want to see the full XML input/output, you can turn on debug mode
$MS->useDebugMode(false);

/*
There are several methods available in the QuickBooks_MerchantService class. 
The most common methods are described below: 

 - authorize() 
    This authorizes a given amount against the a credit card. It is important 
    to note that this *does not* actually charge the credit card, it just 
    "reserves" the amount on the credit card and guarentees that if you do a 
    capture() on the same credit card within X days, the funds will be 
    available. 
    
    Authorizations are used in situations where you want to ensure the money 
    will be availble, but not actually charge the card yet. For instance, if 
    you have an online shopping cart, you should authorize() the credit card 
    when the customer checks out. However, because you might not have the item 
    in stock, or there might be other problems with the order, you don't want 
    to actually charge the card yet. Once the order is all ready to ship and 
    you've made sure there's no problems with it, you should issue a capture() 
   	using the returned transaction information from the authorize() to actually 
   	charge the credit card. 
    
 - capture()   
 - charge()
 - void()
 - refund()
 - voidOrRefund() 
 - openBatch()
 - closeBatch()

*/

//POST Parameters
if(isset($_POST['fname']) && isset($_POST['lname']) && isset($_POST['num']) && isset($_POST['expM']) && isset($_POST['expY']) && isset($_POST['cvv']) && isset($_POST['amount']) && isset($_POST['type'])) {
	// Now, let's create a credit card object, and authorize an amount agains the card
	$number = $_POST['num'];
	$expyear = $_POST['expY'];
	$expmonth = $_POST['expM'];
	$cvv = $_POST['cvv'];
	$tax = $_POST['tax'];
	$type = $_POST['type'];
	
	if($_POST['amount'] != "") {
		$amount = $_POST['amount'];
	} else {
		echo 'Please enter a dollar amount.';
		exit;
	}
	
	//Racer Information
	$firstName = makeSQLSafe($mysqli,$_POST['fname']);
	$lastName = makeSQLSafe($mysqli,$_POST['lname']);
	$name = $firstName." ".$lastName;
	$driver = makeSQLSafe($mysqli,$_POST['driver']);
	$racerID = makeSQLSafe($mysqli,$_POST['rid']);
	$email = makeSQLSafe($mysqli,$_POST['email']);
	$address = makeSQLSafe($mysqli,$_POST['address']);
	$city = makeSQLSafe($mysqli,$_POST['city']);
	$state = makeSQLSafe($mysqli,$_POST['state']);
	$zip = makeSQLSafe($mysqli,$_POST['zip']);
	$phone = makeSQLSafe($mysqli,$_POST['phone']);
	$passType = makeSQLSafe($mysqli,$_POST['passType']);
	$location = makeSQLSafe($mysqli,$_POST['location']);
	$notes = makeSQLSafe($mysqli,$_POST['notes']);
	//Refund
	$passNum = makeSQLSafe($mysqli,$_POST['passNum']);
	
} else {
	echo "An error has occurred with this transaction request. Missing information.";
	exit;
}

// Create the CreditCard object
$Card = new QuickBooks_MerchantService_CreditCard($name, $number, $expyear, $expmonth, $cvv);

//New pass transaction
if($type == "newpass" || $type == "schedule" || $type == "rerace") {
	if ($Transaction = $MS->authorize($Card, $amount, $tax)) {
		//print('Card authorized!' . "\n");
		//print_r($Transaction);	
		
		// 	Every time the MerchantService class returns a $Transaction object to you, 
		// 	you should store the returned $Transaction. You'll need the returned 
		// 	$Transaction object (or at the very least the data contained therein) in 
		// 	order to push these transactions to QuickBooks, to actually capture the 
		// 	funds, to issue a refund, or to issue a void. 
		// 	
		// 	There are several convienence methods to convert the $Transaction object to 
		// 	more storage-friendly formats if you would prefer to use these: 
		
		// Get the transaction as a string which can later be turned back into a transaction object
		$str = $Transaction->serialize(); 
		//print('Serialized transaction: ' . $str . "\n\n");
		
		// Now convert it back to a transaction object
		$Transaction = QuickBooks_MerchantService_Transaction::unserialize($str);
		
		// ... maybe you'd rather convert it to an array? 
		$arr = $Transaction->toArray();
		//print('Array transaction: '); 
		//print_r($arr);
		//print("\n\n");
		
		// ... and back again?
		$Transaction = QuickBooks_MerchantService_Transaction::fromArray($arr);
		
		// ... or an XML document?
		$xml = $Transaction->toXML();
		//print('XML transaction: ' . $xml . "\n\n");
		
		// ... and back again? 
		$Transaction = QuickBooks_MerchantService_Transaction::fromXML($xml);
		
		// How about XML that can be used in a qbXML SalesReceiptAdd request?
		$qbxml = $Transaction->toQBXML();
		//print('qbXML transaction info: ' . $qbxml . "\n\n");
		
		if ($Transaction = $MS->capture($Transaction, $amount)) {
			
			// Let's print that qbXML bit again because it'll have more data now
			$qbxml = $Transaction->toQBXML();
			//print('qbXML transaction info: ' . $qbxml . "\n\n");		
			
			//If racer profile is not found, create a profile.
			if($racerID == "") {
				$mysqli->query("INSERT INTO `LARX_racer_profiles` (`first_name`,`last_name`,`address`,`city`,`state`,`zip_code`,`phone`,`email_address`,`timestamp`) 
				VALUES ('$firstName','$lastName','$address','$city','$state','$zip','$phone','$email',NOW())");	
				$lastID = $mysqli->insert_id;
				$racerID = $lastID;
			}
			
			//Generate race pass number and id hash number
			$racePass = generateRacePass($mysqli);
			$raceHash = generateRaceHash($mysqli);
			
			if($_POST['type'] == "newpass") {
				//Create Pass
				$mysqli->query("INSERT INTO `LARX_race_passes` (`profile_id`,`buyer_name`,`driver_name`,`track_location`,`pass_number`,`pass_type`,`pass_status`,`pass_hash`,`timestamp`) VALUES ('$racerID','$name','$driver','$location','$racePass','$passType','valid','$raceHash',NOW())");
				//Log Transaction
				$mysqli->query("INSERT INTO `LARX_transaction_log` (`profile_id`,`pass_num`,`amount`,`tax`,`type`,`timestamp`) VALUES ('$racerID','$racePass','$amount','$tax','$type',NOW())");
				echo '1';
			} else if($_POST['type'] == "schedule") {
				//Log Transaction
				$mysqli->query("INSERT INTO `LARX_transaction_log` (`profile_id`,`amount`,`tax`,`type`,`notes`,`timestamp`) VALUES ('$racerID','$amount','$tax','$type','$notes',NOW())");
				echo '1';
			} else if($_POST['type'] == "rerace") {
				//Create Re-Race Pass
				$mysql->query("INSERT INTO `LARX_race_passes` (`profile_id`,`buyer_name`,`driver_name`,`track_location`,`pass_number`,`pass_type`,`pass_status`,`re_race_pass`,`pass_hash`,`timestamp`) VALUES ('$racerID','$name','$driver','$location','$racePass','$passType','valid',1,'$raceHash',NOW())");
				//Log Transaction
				$mysqli->query("INSERT INTO `LARX_transaction_log` (`profile_id`,`pass_num`,`amount`,`tax`,`type`,`timestamp`) VALUES ('$racerID','$racePass','$amount','$tax','$type',NOW())");
				echo '1';	
			}
		} else {
			print('Capture ' . $MS->errorNumber() . ': ' . $MS->errorMessage() . "\n");
		}
	} else {
		print('Authorization ' . $MS->errorNumber() . ': ' . $MS->errorMessage() . "\n");
	}
}
//CHARGE REFUNDS
elseif($type == "refund") {

	if ($Transaction = $MS->refund($Card, $amount)) {
		//Create Re-Race Pass
		$fetchPass = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `pass_number` = '$passNum' LIMIT 1");
		if($fetchPass->num_rows == 1) {
			$pass = $fetchPass->fetch_array();
			//Update Pass Status
			$mysqli->query("UPDATE `LARX_race_passes` SET `pass_status` = 'void' WHERE `id` = '".$pass['id']."' LIMIT 1");
			//Log Transaction
			$mysqli->query("INSERT INTO `LARX_transaction_log` (`profile_id`,`pass_num`,`amount`,`tax`,`type`,`timestamp`) VALUES ('$racerID','".$pass['pass_number']."','$amount','$tax','$type',NOW())");
		}
		echo "1";
	} else {
		print('Refund ' . $MS->errorNumber() . ': ' . $MS->errorMessage() . "\n");
	}
}

?>

