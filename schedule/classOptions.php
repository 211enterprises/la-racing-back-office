<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$h = makeSQLSafe($mysqli,$_GET['h']);
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$h' LIMIT 1");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Upcoming Class Dates</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script src="test.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1>Class Options:</h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php if($classQuery->num_rows > 0) {
	$class = $classQuery->fetch_array();
	echo '<h3 style="text-align:center;">'.date("l - F jS, Y",strtotime($class['date'])).' - '.date("h:i A",strtotime($class['time'])).'</h3>';
	echo '<div data-role="controlgroup">';
	echo '<a href="/office/assignracers/'.$class['class_hash'].'" data-role="button" data-icon="arrow-r" data-iconpos="right" data-theme="a">Assign Racers</a>';
	echo '<a href="/office/schedule/editClass.php?h='.$class['class_hash'].'" data-rel="dialog" data-role="button" data-theme="a" data-icon="arrow-r" data-iconpos="right">Edit Class</a>';
	echo '<a href="/office/class/'.$class['class_hash'].'" data-role="button" data-theme="b" data-icon="star" data-iconpos="right">Start Class</a>';
	echo '</div>';
	
} else echo '<h1>No Class Date Found.</h1>'; ?>
   		
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>