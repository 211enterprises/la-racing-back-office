<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$h = makeSQLSafe($mysqli,$_GET['h']);
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$h' LIMIT 1");
	$class = $classQuery->fetch_array();
	
	if(substr($class['time'],0,2) <= 12) $classHour = substr($class['time'],0,2); else $classHour = substr($class['time'],0,2) - 12;
	
	//TRACK QUERY
	$trackQuery = $mysqli->query("SELECT * FROM `LARX_track_locations` ORDER BY `track_name` ASC");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Edit Class Dates</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script src="test.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($classQuery->num_rows > 0) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo date("m/d/Y",strtotime($class['date'])).' - '.date("h:i A",strtotime($class['time'])); ?></h1>
	   <a href="" data-theme="b" data-role="button" data-iconpos="left" data-icon="delete" id="classDelete">Delete Class</a>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php if($classQuery->num_rows > 0) { ?>
	
	<form action="" method="post">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<!--CLASS DATE-->
				<label>Class Date:</label>
				<fieldset data-role="controlgroup" data-type="horizontal">
					
					<select name="classMonth" id="classMonth">
						<option value="">M:</option>
				<?php for($m = 1; $m <= 12; $m++) { ?>
						<option value="<?php echo str_pad($m,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['date'],5,2) == $m) echo 'selected="selected"'; ?>><?php echo str_pad($m,2,0,STR_PAD_LEFT); ?></option>
				<?php } ?>
					</select>
					
					<select name="classDay" id="classDay">
						<option value="">D:</option>
				<?php for($d = 1; $d <= 31; $d++) { ?>
						<option value="<?php echo str_pad($d,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['date'],8,2) == $d) echo 'selected="selected"'; ?>><?php echo str_pad($d,2,0,STR_PAD_LEFT); ?></option>
				<?php } ?>
					</select>
					
					<select name="classYear" id="classYear">
						<option value="">Y:</option>
						<option value="<?php echo date("Y"); ?>" <?php if(substr($class['date'],0,4) == date("Y")) echo 'selected="selected"'; ?>><?php echo date("Y"); ?></option>
						<option value="<?php echo date("Y") + 1; ?>" <?php if(substr($class['date'],0,4) == date("Y") + 1) echo 'selected="selected"'; ?>><?php echo date("Y") + 1; ?></option>
					</select>
					
				</fieldset>
			</li>
			<li data-role="fieldcontain">
				<!--CLASS TIME-->
				<label>Class Time:</label>
				<fieldset data-role="controlgroup" data-type="horizontal">
					
					<select name="classTimeH" id="classTimeH">
						<option value="">Hour:</option>
				<?php for($th = 1; $th <= 12; $th++) { ?>
						<option value="<?php echo str_pad($th,2,0,STR_PAD_LEFT); ?>" <?php if($classHour == $th) echo 'selected="selected"'; ?>><?php echo str_pad($th,2,0,STR_PAD_LEFT); ?></option>
				<?php } ?>
					</select>
					
					<select name="classTimeM" id="classTimeM">
						<option value="">Mins:</option>
				<?php for($tm = 0; $tm <= 59; $tm++) { ?>
						<option value="<?php echo str_pad($tm,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['time'],3,2) == $tm) echo 'selected="selected"'; ?>><?php echo str_pad($tm,2,0,STR_PAD_LEFT); ?></option>
				<?php } ?>
					</select>
					
					<select name="classTimeP" id="classTimeP">
						<option value="">Period:</option>
						<option value="AM" <?php if(substr($class['time'],0,2) < 12) echo 'selected="selected"'; ?>>AM</option>
						<option value="PM" <?php if(substr($class['time'],0,2) >= 12) echo 'selected="selected"'; ?>>PM</option>
					</select>
					
				</fieldset>
			</li>
			<li data-role="fieldcontain">
				<!--CLASS LIMIT-->
				<label>Class Limit:</label>
				<fieldset data-role="controlgroup">
					<input type="range" name="classLimit" id="classLimit" data-highlight="true" min="0" max="50" value="<?php echo $class['class_limit']; ?>" />			
				</fieldset>
			</li>
			<li data-role="fieldcontain">
				<!--CLASS TRACK-->
				<label>Track Location:</label>
				<fieldset data-role="controlgroup">
					<select name="classLimit" id="classLimit">
						<option value="">Track:</option>
				<?php while($track = $trackQuery->fetch_assoc()) { ?>
						<option value="<?php echo $track['track_id']; ?>" <?php if($class['track_location'] == $track['track_id']) echo 'selected="selected"'; ?>><?php echo $track['track_name']; ?></option>
				<?php } ?>
					</select>			
				</fieldset>
			</li>
			<li data-role="fieldcontain">
				<button type="submit" data-theme="a" data-icon="arrow-r" data-iconpos="right">Edit Class</button>
			</li>
			
		</ul>
	</form>
	
<?php } else echo '<h1 style="text-align:center;">No Class Date Found.</h1>'; ?>
   		
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>