<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Transactions</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1>Transaction Type:</h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
	
	<form action="" method="post">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<label for="transType">Transaction Type:</label>
				<select name="transType" id="transType">
					<optgroup label="Choose a transaction type:">
						<option value="">Select:</option>
						<option value="newPass">New Race Pass</option>
						<option value="rerace">Re-Race Pass</option>
						<option value="schedule">Cancelation Charge</option>
						<option value="refund">Refund Race Pass</option>
					</optgroup>
				</select>
			</li>
			<!--<li data-role="fieldcontain">
				<button type="submit" id="transTypeBtn" data-theme="a" data-icon="arrow-r" data-iconpos="right">Go</button>
			</li>-->
		</ul>
	</form>
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$mysqli->close();	
?>