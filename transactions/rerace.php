<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` ORDER BY `first_name` ASC");
	$passQuery = $mysqli->query("SELECT * FROM `LARX_classes` ORDER BY `class_name` ASC LIMIT 1");

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Transactions</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1>Customer Re-Race Pass:</h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
	
	<form action="" method="post">
		<ul data-role="listview" id="transForm">
			<li data-role="fieldcontain" class="cardErrors">
				<div class="ui-grid-solo"></div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-solo">
					<label for="racerProfile">Racer Profile:</label><br />
					<select name="racerProfile" id="racerProfile">
						<optgroup label="Choose a racer profile:">
							<option value="">Select:</option>
					<?php while($racer = $racerQuery->fetch_array()) { ?>
							<option value="<?php echo $racer['id']; ?>"><?php echo $racer['id']." - ".$racer['first_name']." ".$racer['last_name']; ?></option>
					<?php } ?>
						</optgroup>
					</select>
					<div class="racer-flag" id="alertFlag"><img src="/office/global/images/red_flag.png" alt="Racer Red Flagged" /></div>
				</div>
			</li>
			<li data-listdivider="true" data-theme="a">Customer Information</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<label for="firstName">First Name:</label><br />
						<input type="text" name="firstName" id="firstName" />
					</div>
					<div class="ui-block-b">
						<label for="lastName">Last Name:</label><br />
						<input type="text" name="lastName" id="lastName" />
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-solo">
					<label for="address" style="width:100%; text-align:left;">Address:</label><br />
					<input type="text" name="address" id="address" />
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<label for="city">City:</label><br />
						<input type="text" name="city" id="city" />
					</div>
					<div class="ui-block-b">
						<label for="state">State:</label><br />
						<input type="text" name="state" id="state" />
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<label for="zip">Zip Code:</label><br />
						<input type="text" name="zip" id="zip" />
					</div>
					<div class="ui-block-b">
						<label for="phone">Phone:</label><br />
						<input type="text" name="phone" id="phone" />
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-solo">
					<label for="email" style="width:100%; text-align:left;">Email:</label><br />
					<input type="text" name="email" id="email" />
				</div>
			</li>
			<li data-listdivider="true" data-theme="a">Pass Information</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-solo">
					<label for="passType">Pass Type:</label><br />
					<select name="passType">
						<option value="">Select:</option>
				<?php 
					while($pass = $passQuery->fetch_array()) {
						echo '<option value="'.$pass['id'].'" selected="selected">'.$pass['class_name'].'</option>';
					}
				?>
					</select>
				</div>
				<div class="ui-grid-solo">
					<label for="driverName" style="width:100%; text-align:left;">Driver Name:</label><br />
					<input type="text" name="driverName" id="driverName" />
				</div>
				<div class="ui-grid-solo">
					<label for="passPrice" style="width:100%; text-align:left;">Pass Price:</label><br />
					<input type="text" name="passPrice" id="passPrice" value="100.00" />
				</div>
			</li>
			<li data-listdivider="true" data-theme="a">Credit Card Information</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<label for="cardNum">Card Number:</label><br />
						<input type="text" name="cardNum" id="cardNum" />
					</div>
					<div class="ui-block-b">
						<label for="cardExpM">Card Expiration:</label><br />
						<fieldset data-role="controlgroup" id="cardExp" data-type="horizontal" style="width:240px;">
							<select name="cardExpM" id="cardExpM">
								<optgroup label="Expiration Month:">
									<option value="">Mon:</option>
							<?php for($m = 01; $m <= 12; $m++) { ?>
									<option value="<?php echo $m; ?>"><?php echo $m; ?></option>
							<?php } ?>
								</optgroup>
							</select>
							<select name="cardExpY" id="cardExpY">
								<optgroup label="Expiration Year:">
									<option value="">Year:</option>
									<option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
									<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
									<option value="<?php echo date("Y") + 2; ?>"><?php echo date("Y") + 2; ?></option>
									<option value="<?php echo date("Y") + 3; ?>"><?php echo date("Y") + 3; ?></option>
									<option value="<?php echo date("Y") + 4; ?>"><?php echo date("Y") + 4; ?></option>
									<option value="<?php echo date("Y") + 5; ?>"><?php echo date("Y") + 5; ?></option>
								</optgroup>
							</select>
						</fieldset>
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						<label for="cardCode">Card Security Code:</label><br />
						<input type="text" name="cardCode" id="cardCode" />
					</div>
					<div class="ui-block-b"></div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-solo">
					<button type="submit" id="reraceBtn" data-theme="a" data-icon="arrow-r" data-iconpos="right">Process Re-Race Pass</button>
				</div>
			</li>
		</ul>
	</form>
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$mysqli->close();	
?>