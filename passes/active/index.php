<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$passQuery = $mysqli->query("SELECT `profile_id`,`buyer_name`,`driver_name`,`pass_number`,`pass_status`,`pass_hash`,`timestamp` FROM `LARX_race_passes` WHERE `pass_status` = 'valid' AND `class_id` = 0 ORDER BY `timestamp` ASC");

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Active Race Passes</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript">
	$('#activePasses').bind("pageshow",function() {
		$('ul').listview('refresh');
	});
</script>
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="activePasses">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
	   <a href="/office/schedule/" data-role="button" data-icon="home" data-iconpos="left" data-transition="flip">Home</a>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($passQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="processPasses" data-inset="true" data-filter="true" data-filter-placeholder="Search by name or profile #...">
   			<li data-listdivider="true" data-theme="a">Active Race Passes:</li>
   	<?php while($pass = $passQuery->fetch_assoc()) { ?>
   			<li>
				<a href="/office/passes/viewPass.php?p=<?php echo $pass['pass_hash']; ?>" data-rel="dialog" data-transition="pop"><?php echo $pass['pass_number']." - "; if($pass['buyer_name'] != NULL) echo $pass['buyer_name']; else echo $pass['driver_name']; ?></a>
				<?php if($racer['red_flag'] == 1) echo '<div class="racer-flag"><img src="/office/global/images/red_flag.png" alt="Racer Red Flagged" /></div>'; ?>
			</li>
   	<?php } ?>
   		</ul>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo passesFooter("active"); ?>
      
</div>

</body>
</html>
<?php
$racersQuery->close();
$mysqli->close();	
?>